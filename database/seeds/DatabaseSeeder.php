<?php

use Illuminate\Database\Seeder;
use App\TripType;
use App\Location;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
    	foreach (['Екскурсія', 'Активний відпочинок', 'Подорож', 'Похід', 'Релакс-тур', 'Піший тур'] as $key => $value) {
    		TripType::create(['title' => $value, 'slug' => str_slug($value, '-')]);
    	}

    	factory(App\Location::class, 20)->create();

        factory(App\User::class, 'i', 1)->create();

    	factory(App\User::class, 5)->create();

        App\User::all()->each(function($u) {

    		$t = factory(App\Tour::class, 5)->create()->each(function($tt) {
    			//$tt->primary_image_id = factory(App\Image::class, 'nature')->create()->id;

                $trip_types = TripType::inRandomOrder()->take(rand(1,3))->get();
                $tt->trip_types()->saveMany($trip_types);
    			
    			$loc = Location::inRandomOrder()->take(rand(1,5))->get();
		    	$tt->to()->saveMany($loc);

		    	$prices = factory(App\Price::class, rand(2,4))->create();
		    	
		    	$tt->prices()->saveMany($prices);

    			$tt->save();
    		});

    		$u->save();

		    $u->tours()->saveMany($t);

		});



    }
}
