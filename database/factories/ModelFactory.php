<?php

use Faker\Factory as Faker;
use Illuminate\Http\UploadedFile;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


$factory->define(App\User::class, function ($faker) {
	$faker = Faker::create('ru_RU');
    $email = null;
    if (\App\User::all()->count() == 0)
    {
        $email = 'krivich.igor@gmail.com';
    }
    static $password;

    $path = $faker->image(sys_get_temp_dir().'/', 200, 200, 'people');
    $file = new UploadedFile($path, 'name.jpg', 'image/img', 111, null, true);

    return [
        'name' => ($email) ? 'Кривич Игорь' : $faker->firstName . " " . $faker->lastName,
        'email' => ($email) ? : $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'upload_image' => $file,
        'remember_token' => str_random(10),
        'phone' =>  '(063) 691-19-85',
    ];
});





$factory->defineAs(App\User::class, 'i', function ($faker) {
    $faker = Faker::create('ru_RU');
    static $password;

    $path = $faker->image(sys_get_temp_dir().'/', 200, 200, 'people');
    $file = new UploadedFile($path, 'name.jpg', 'image/img', 111, null, true);

    return [
        'name' => 'Кривич Игорь',
        'email' => 'krivich.igor@gmail.com',
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'upload_image' => $file,
        'phone' =>  '(063) 691-19-85',
    ];
});





$factory->define(App\Tour::class, function ($faker) {
	$faker = Faker::create('ru_RU');
	$program = [];
	foreach (range(0,3) as $key => $value) {
		$program[] = array(
			'title'		  => 'День ' . ($key + 1),
			'description' => $faker->realText(rand(2,7) * 60)
		);
	}
    $title = $faker->unique()->sentence(rand(2,4));

    $path = $faker->image(sys_get_temp_dir().'/', 420, 315, 'nature');
    $file = new UploadedFile($path, 'name.jpg', 'image/img', 111, null, true);
    $excluded = [];
    $included = [];
    foreach (range(1, rand(2,7)) as $key => $value) {
        $included[] = ['title' => $faker->sentence()];
    }
    foreach (range(1, rand(2,7)) as $key => $value) {
        $excluded[] = ['title' => $faker->sentence()];
    }     
    return [
        'title' => $title,
        'slug' =>  str_slug($title, '-'),
        'days' =>  rand(1,10),
        'starts_at' =>  $faker->dateTimeBetween('+10 days', '+1 year')->format('Y-m-d H:i:s'),
        'description' => $faker->unique()->realText(rand(2,5) * 40),
        'program' => $program,
        'upload_image'   => $file,
        'included' => $included, 
        'excluded' => $excluded, 
        'from_id' => \App\Location::where('id', '<', 5)->inRandomOrder()->take(1)->get()->first()->id,
    ];
});






$factory->define(App\Location::class, function ($faker) {
	$faker = Faker::create('ru_RU');
    $title = $faker->unique()->city;
    return [
        'name' => $title,
        'slug' => str_slug($title, '-'),
        'country' => $faker->country,
        'latitude' => $faker->unique()->latitude,
        'longitude' => $faker->unique()->longitude,
    ];
});




$factory->define(App\Price::class, function ($faker) {
	$faker = Faker::create('ru_RU');
	$a = [
		'Одномісний номер, 1 особа',
		'Двомісний номер, 1 особа',
		'Тримісний номер, 1 особа',
		'Все включено',
	];
	$k = array_rand($a);
    return [
        'price' => 400 + rand(10, 50) * 30 + rand(0,1) * 1000,
        'title' => $a[$k],
    ];
});

