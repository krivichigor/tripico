<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourTripTypePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_trip_type', function (Blueprint $table) {
            $table->integer('tour_id')->unsigned()->index();
            $table->foreign('tour_id')->references('id')->on('tours')->onDelete('cascade');
            $table->integer('trip_type_id')->unsigned()->index();
            $table->foreign('trip_type_id')->references('id')->on('trip_types')->onDelete('cascade');
            $table->primary(['tour_id', 'trip_type_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tour_trip_type');
    }
}
