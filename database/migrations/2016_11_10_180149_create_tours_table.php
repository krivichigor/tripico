<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->datetime('starts_at');
            $table->text('description')->nullable();
            $table->integer('days')->unsigned()->nullable();
            $table->integer('from_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('image')->nullable();
            $table->string('image_thumb')->nullable();
            $table->longText('program')->nullable();
            $table->text('included')->nullable();
            $table->text('excluded')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
