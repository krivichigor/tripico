<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/tours/{filter?}', 'TourController@index')->name('tour.list')->where('filter', '(.*)?');

Route::get('/tour/{id}', 'TourController@show')->name('tour.show');
Route::post('/tour/{id}/registration', 'TourController@storeRegistration')->name('tour.registration');



Route::group(['prefix' => 'me', 'middleware' => 'auth', 'as' => 'me.'], function () {
	Route::get('/', 'MeController@index')->name('index');
	Route::get('/registrations', 'MeController@registrations')->name('registrations');
	Route::resource('tour', 'TourController', [
		'except' => [ 'index', 'show', 'makeRegistration'], 
		'names' => [
	    	//'create' => 'photo.build'
		]
	]);
});

Route::get('/auth/fb/callback', 'Auth\FacebookController@handleProviderCallback')->name('auth.facebook.callback');
Route::get('/auth/fb', 'Auth\FacebookController@redirectToProvider')->name('auth.facebook');
Auth::routes();
