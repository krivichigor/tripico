<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Requests;
use Socialite;
use App\User;
use App\Image;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class FacebookController extends Controller
{


	public function redirectToProvider()
	{
		return Socialite::driver('facebook')->redirect();
		//->scopes(['publish_actions', 'manage_pages', 'publish_pages'])
	}

	/**
	 * Obtain the user information from GitHub.
	 *
	 * @return Response
	 */
	public function handleProviderCallback()
	{
		$s_user = Socialite::driver('facebook')->user();

		if ($user = User::where('email', $s_user->email)->get()->first()){
			//$user->update(['facebook_token' => $s_user->token, 'facebook_expires_in' => $s_user->expiresIn,]);
		}
		else{
			
			$img_url = str_replace('?type=normal', '?type=square&width=360&height=360', $s_user->avatar);
			
			$user = new User;
			$user->email = $s_user->email;
			$user->image_id = Image::create(['url' => $img_url])->id;
			$user->name = $s_user->name;
			$user->password = bcrypt($s_user->id . $s_user->email);
			$user->save();
		}

		Auth::login($user, true);

		return redirect('/#');
	}

}
