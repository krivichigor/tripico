<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tour;
use App\Location;
use App\TripType;
use App\Helpers\Filter;
use App\Price;
use Illuminate\Support\Facades\Auth;

class TourController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($filter_string = null)
    {
        
        $filter = null;
        
        $data['filter'] = [];
        $h1_text = 'Всі тури по Україні';
        if ($filter_string){
            $filter = new Filter($filter_string);
            if ($filter->error)
                return abort('404');
            $data['filter'] = $filter->get();

            $data['filter'] = $filter->GetFilterObjects();
            $filterObj = $data['filter'];
            $h1_text  = (isset($filterObj['type'])) ? $filterObj['type']->title         : 'Всі тури';
            $h1_text .= (isset($filterObj['to']))   ? ' в ' . $filterObj['to']->name    : ' по Україні';
            $h1_text .= (isset($filterObj['from']))  ? ' з ' . $filterObj['from']->name : '';
        }


        $data['tours'] = Tour::filtered($filter)->get();

        $data['h1_text'] = $h1_text;



        $data['location_to'] = Location::getAllWithRelationCount('tours_to');
        $data['location_from'] = Location::getAllWithRelationCount('tours_from');
        $data['trip_types'] = TripType::getAllWithRelationCount('tours');

        return view('tours.list.index', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!$data['tour'] = Tour::find($id))
            return abort('404');

        return view('tours.show.index', $data);
    }



    public function storeRegistration(Request $request, $id)
    {
        if (!$tour = Tour::find($id))
            return abort('404');

        if ($tour->registrations()->create($request->all()))
            \Session::flash('flash_message', 'Вітаємо, ви зареєструвались на тур ' . $tour->title);

        return redirect()->route('tour.show', $tour);
    }





    /**
     *    Admin part
     *
    */


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['tour'] = null;

        $data['title'] = "Створення нового туру";
        $data['arr_to'] = Location::pluck('name', 'id');
        $data['arr_trip_types'] = TripType::pluck('title', 'id');

        return view('tours.admin.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        if ($tour = Auth::user()->tours()->create($request->all()))
            \Session::flash('flash_message', 'Тур було створено!');

        $tour->updateManyRelations($request);

        return redirect()->route('me.tour.edit', $tour->id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (!$data['tour'] = Tour::WithArchive()->find($id))
            return abort('404');

        $data['title'] = "Зміна тура " . $data['tour']->title;

        $data['arr_to'] = Location::pluck('name', 'id');
        $data['arr_trip_types'] = TripType::pluck('title', 'id');
        

        return view('tours.admin.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        if (!$tour = Tour::WithArchive()->find($id))
            return abort('404');

        if ($tour->update($request->all()))
            \Session::flash('flash_message', 'Тур було оновлено!');

        $tour->updateManyRelations($request);

        return redirect()->back();


    }

    

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
