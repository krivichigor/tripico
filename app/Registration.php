<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tour;
use App\Price;

class Registration extends Model
{

	protected $fillable = [
        'tour_id',  'user_id', 'name', 'email', 'phone', 'people_count', 'price', 'price_title', 'tour_json', 'comment', 'option'
    ];

    protected $guarded = [
        '*'
    ];

    public function tour()
    {
        return $this->hasOne('App\Tour', 'id', 'tour_id');
    }


    protected static function boot()
    {
        parent::boot();

        /*static::addGlobalScope('order_desc', function(Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });*/
    }

    public function setTourIdAttribute($value)
    {
    	$this->tour_json = Tour::find($value)->toArray();
    	$this->attributes['tour_id'] = $value;
    }

    public function setOptionAttribute($value)
    {
    	$price = Price::find($value);

        $this->attributes['price'] = $price->price;
        $this->attributes['price_title'] = $price->title;

    }

    public function setTourJsonAttribute($value)
    {
    	$this->attributes['tour_json'] = json_encode($value);
    }

    public function getTourJsonAttribute($value)
    {
    	return json_decode($value);
    }


    
}
