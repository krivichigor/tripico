<?php

namespace App\Helpers;

use App\Location;
use App\TripType;

class Filter
{
	var $filter_names = [
		'from',
        'to',
        'type',
        'when'
	];



	var $string = null;
	var $filter_array = null;
	var $error = null;

	public function __construct($string = null)
	{
		$this->string = $string;

		foreach (explode('/', $string) as $key => $value) {
             $e = explode('=', $value);

             if ((in_array($e[0], $this->filter_names)) and (count($e) == 2))
             	$this->filter_array[$e[0]] = $e[1];
             else
             	$this->error = true;
        }
	}


    public function GetFilterObjects(){
    	$a = [];
    	$f = $this->filter_array;
    	if (isset($f['from']))
    		$a['from'] = Location::whereSlug($f['from'])->get()->first();
    	if (isset($f['to']))
    		$a['to'] = Location::whereSlug($f['to'])->get()->first();
    	if (isset($f['type']))
    		$a['type'] = TripType::whereSlug($f['type'])->get()->first();
    	return $a;
    }

	public function get()
	{
		return $this->filter_array;
	}

	public function getWithout($column)
	{
		$f = $this->filter_array;
		unset($f[$column]);

		return $f;
	}

}