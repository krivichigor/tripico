<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use \KodiComponents\Support\Upload;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'upload_image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'image' => 'image',
        'image_thumb' => 'image',
    ];

    public function getUploadSettings()
    {
        return [
            'image' => [
                'orientate' => [],
                'fit' => [200, 200, function ($constraint) {
                    $constraint->upsize();
                    $constraint->aspectRatio();
                }]
            ],
            'image_thumb' => [
                'orientate' => [],
                'fit' => [40, 40, function ($constraint) {
                    $constraint->upsize();
                    $constraint->aspectRatio();
                }]
            ]
        ];
    }


    public function tours()
    {
        return $this->hasMany('App\Tour', 'user_id');
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     */
    public function setUploadImageAttribute(\Illuminate\Http\UploadedFile $file = null)
    {
        if (is_null($file)) {
            return;
        }

        foreach ($this->getUploadFields() as $field) {
            $this->{$field.'_file'} = $file;
        }
    }



}
