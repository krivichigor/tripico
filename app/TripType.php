<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripType extends Model
{
    protected $fillable = [
        'title', 'description', 'image_id'
    ];

    protected $guarded = [
        '*'
    ];

    public function tours()
    {
        return $this->belongsToMany('App\Tour');
    }

    public static function getAllWithRelationCount($rel = 'tours')
    {
        return  self::whereHas($rel)
            ->select('title', 'slug')
            ->withCount($rel . ' AS ')
            ->orderBy('_count', 'desc')
            ->get();
    }
}
