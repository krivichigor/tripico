<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use App\Helpers\Filter;
use Collective\Html\Eloquent\FormAccessible;

class Tour extends Model
{
    use FormAccessible;
    use \KodiComponents\Support\Upload;

    protected $fillable = [
        'title', 'description',  'program', 'slug', 'starts_at', 'days', 'from_id', 'image', 'image_thumb', 'upload_image', 'included', 'excluded'
    ];

    protected $guarded = [
        '*'
    ];

    protected $dates = [
        'starts_at'
    ];

    protected $casts = [
        'image' => 'image',
        'image_thumb' => 'image',
    ];

    public function getUploadSettings()
    {
        return [
            'image' => [
                'orientate' => [],
                'fit' => [420, 315, function ($constraint) {
                    $constraint->upsize();
                    $constraint->aspectRatio();
                }]
            ],
            'image_thumb' => [
                'orientate' => [],
                'fit' => [75, 75, function ($constraint) {
                    $constraint->upsize();
                    $constraint->aspectRatio();
                }]
            ]
        ];
    }



    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('current', function(Builder $builder) {
            $builder->where('starts_at', '>', Carbon::now());
        });

        static::addGlobalScope('order', function(Builder $builder) {
            $builder->orderBy('starts_at', 'asc');
        });
    }


    /*
    		Relations
    */

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function trip_types()
    {
        return $this->belongsToMany('App\TripType');
    }

    public function from()
    {
        return $this->hasOne('App\Location', 'id', 'from_id');
    }


    		// Many

    public function to()
    {
        return $this->belongsToMany('App\Location');
    }
    
    public function prices()
    {
        return $this->hasMany('App\Price', 'tour_id')->orderBy('price', 'asc');
    }

    public function registrations()
    {
        return $this->hasMany('App\Registration', 'tour_id')->orderBy('created_at', 'desc');;
    }


    /*
            Scopes
    */


    public function ScopeWithArchive($query)
    {
        return $query->withoutGlobalScope('current');
    }

    public function ScopeOnlyArchive($query)
    {
        return $query->withoutGlobalScope('current')->where('starts_at', '<', Carbon::now());
    }

    public function ScopeReverse($query)
    {
        return $query->orderBy('starts_at', 'desc');;
    }

    public function ScopeFiltered($query, $filter = null)
    {
        if (!$filter)
            return $query;

        foreach ($filter->get() as $key => $value) {
            $f_name = camel_case('filter_' . $key);
            
            $query->$f_name($value);
        }
        return $query;
    }

    public function ScopeFilterFrom($query, $value)
    {
        return $query->whereHas('from', function ($query) use ($value) {
                $query->whereSlug($value);
            });
    }

    public function ScopeFilterTo($query, $value)
    {
        return $query->whereHas('to', function ($query) use ($value) {
                $query->whereSlug($value);
            });
    }

    public function ScopeFilterType($query, $value)
    {
        return $query->whereHas('trip_types', function ($query) use ($value) {
                $query->whereSlug($value);
            });
    }


    /*
    		Getters & setters
    */
    public function setStartsAtAttribute($value)
    {
        $this->attributes['starts_at'] = Carbon::parse($value)->endOfDay();
    }


    public function setProgramAttribute($value)
    {
    	$this->attributes['program'] = json_encode($value);
    }

    public function getProgramAttribute($value)
    {
    	return json_decode($value);
    }


    public function setIncludedAttribute($value)
    {
        $this->attributes['included'] = json_encode($value);
    }

    public function getIncludedAttribute($value)
    {
        return json_decode($value);
    }

    public function getIsArchiveAttribute($value)
    {
        return ($this->starts_at < Carbon::now());
    }


    public function setExcludedAttribute($value)
    {
        $this->attributes['excluded'] = json_encode($value);
    }

    public function getExcludedAttribute($value)
    {
        return json_decode($value);
    }


    public function getPriceMinAttribute()
    {
    	return $this->prices()->first();
    }



    /*
            Form Data
    */


    public function formStartsAtAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d');
    }

    public function formToAttribute($value)
    {
        return $this->to()->get()->pluck('id')->toArray();
    }

    public function formFromAttribute($value)
    {
        return $this->from()->get()->pluck('id')->toArray();
    }

    public function formTripTypesAttribute($value)
    {
        return $this->trip_types()->get()->pluck('id')->toArray();
    }


    /**
     * @param \Illuminate\Http\UploadedFile $file
     */
    public function setUploadImageAttribute(\Illuminate\Http\UploadedFile $file = null)
    {
        if (is_null($file)) {
            return;
        }

        foreach ($this->getUploadFields() as $field) {
            $this->{$field.'_file'} = $file;
        }
    }


    public function updateManyRelations($request)
    {
        $this->to()->sync(($request->input('to')) ? : [] );
        $this->trip_types()->sync(($request->input('trip_types')) ? : []);
        $this->prices()->delete();
        foreach ($request->input('prices') as $key => $value) {
            $this->prices()->create($value);
        }
        
    }

}
