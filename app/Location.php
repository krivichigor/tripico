<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{

	protected $fillable = [
        'name', 'latitude', 'longitude', 'country',
    ];

    protected $guarded = [
        '*'
    ];

    public function tours_to()
    {
        return $this->belongsToMany('App\Tour');
    }

    public function tours_from()
    {
        return $this->hasMany('App\Tour', 'from_id');
    }

    public static function getAllWithCountFiltred($rel = 'tours_to', $filter = null)
    {
        return  self
            ::whereHas($rel, function ($query) use ($filter) {
                $query->filter($filter);
            })
            ->select('name', 'slug')
            ->withCount($rel . ' AS ')
            ->orderBy('_count', 'desc')
            ->get();
    }

    public static function getAllWithRelationCount($rel = 'tours_to')
    {
        return  self::whereHas($rel)
            ->select('name', 'slug')
            ->withCount($rel . ' AS ')
            ->orderBy('_count', 'desc')
            ->get();
    }
}
