<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">

    @yield('styles')

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    
</head>
<body>

    <header id="layout-header" class="header-navbar">
        @include('layouts.partials.nav')
    </header>

    

    <div id="layout-content" style="position: relative; min-height: calc(100vh - 120px);">

        <div class="container">
            @include('layouts.partials.flash')
        </div>

        @yield('content')

    </div>
    

    <!-- Footer -->
    <footer id="layout-footer">
        @include('layouts.partials.footer')
    </footer>

    
    <script src="/js/app.js"></script>

    @yield('scripts')
</body>
</html>
