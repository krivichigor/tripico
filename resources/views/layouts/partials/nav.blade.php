<nav id="layout-nav" class="navbar navbar-inverse" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Tripico</a>
        </div>
        <div class="collapse navbar-collapse navbar-main-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="{{ route('tour.list') }}" class="">
                        Всі поїздки
                    </a>
                </li>
                <li class="">
                    <a href="{{ route('me.tour.create') }}">
                        <span class="fui-plus"></span>
                        <span>Додати тур</span>
                    </a>
                </li>


                @if (Auth::guest())
                    <li class="">
                        <a href="{{ route('auth.facebook') }}">fb</a>
                    </li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <img src="{{ Auth::user()->image_thumb_url }}" width="26px" class="img-circle" style="margin-right:10px;vertical-align: top;">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('me.index') }}">
                                    Мої тури
                                </a>
                                <a href="{{ url('/logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    Вийти
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
                {{-- {% if user %}
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hello,   <b class="caret"></b></a>
                    <span class="dropdown-arrow"></span>
                    <ul class="dropdown-menu">
                        <li><a data-request="onLogout">Sign out</a></li>
                    </ul>
                </li>
                {% endif %}   --}}              
            </ul>
        </div>
    </div>
</nav>
