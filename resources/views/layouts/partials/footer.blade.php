<div class="container">
    <nav class="pull-left">
        <ul>
            <li class="active">
                <a href="/">Главная</a>
            </li>
            <li>
                <a href="/courses">Курсы</a>
            </li>
            <li>
                <a href="/schools">Школы</a>
            </li>
        </ul>
    </nav>
    <div class="social-btns pull-right">
        <a href="http://facebook.com/tripicoua" rel="nofollow"><div class="fui-facebook"></div><div class="fui-facebook"></div></a>
    </div>
    <div class="additional-links">

        Если вы представитель курсов - напишите на почту и мы разместим ваш ресурс
    </div>
</div>
