@if (Session::has('flash_message'))
    <div class="alert alert-success alert-dismissible fade in  margin-top-20" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        {{ Session::get('flash_message') }}
    </div>
@endif
@if (Session::has('flash_error'))
    <div class="alert alert-danger alert-dismissible fade in margin-top-20" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        {{ Session::get('flash_error') }}
    </div>
@endif


