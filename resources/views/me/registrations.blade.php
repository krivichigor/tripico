@extends('layouts.app')

@section('content')

<div class="container me-page">
    @include('me._secondary_menu')

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                @if ($registrations->count())
                <table class="table ">
                    <thead>
                        <tr>
                            
                            <td>Назва туру</td>
                            <td>Ім'я</td>
                            <td>Телефон</td>
                            <td>e-mail</td>
                            <td>К-сть людей</td>
                            <td>Ціна</td>
                            <td>Опис ціни</td>
                            <td>Дата</td>
                        </tr>
                    </thead>
                    @foreach($registrations as $item)
                        
                        <tr>
                            
                            
                            
                            
                            <td>
                                <a href="{{ route('tour.show', $item->tour) }}">
                                    <img class="img-circle" src="{{ $item->tour->image_thumb_url }}" width="30" height="30">
                                    {{ $item->tour->title }}
                                </a>
                                
                            </td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->phone }}</td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->people_count }}</td>
                            <td>{{ $item->price }}</td>
                            <td>{{ $item->price_title }}</td>
                            <td>
                                {{ $item->created_at->format('d.m.y') }}
                                <span class="text-muted">{{ $item->created_at->format('H:i') }}</span>
                            </td>

                        </tr>
                        
                    @endforeach
                </table>
                @else
                    <h4 class="text-center margin-top-40 text-muted">Нажаль, в вас ще немає реєстрацій</h4>
                    
                @endif
            </div>
        </div>
    </div>
    
    
</div>

@endsection