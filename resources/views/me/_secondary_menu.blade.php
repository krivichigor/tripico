<?php if(!isset($secondary_menu)) $secondary_menu = null; ?>
<div class="row margin-top-30 margin-bottom-20">
    <div class="col-md-6">
        <h2 class="margin-top-0">{{ (isset($title)) ? $title : ' '}}</h2>
    </div>
    <div class="col-md-6">
        <ul class="nav nav-pills pull-right margin-top-10">
            <li role="presentation" @if($secondary_menu == 'new')class="active" @endif>
                <a href="{{ route('me.tour.create') }}">
                    <span class="fui-plus"></span>
                    <span>Додати тур</span>
                </a>
            </li>
            <li role="presentation" @if($secondary_menu == 'index')class="active" @endif>
                <a href="{{ route('me.index') }}">Мої тури</a>
            </li>
            <li role="presentation" @if($secondary_menu == 'registrations')class="active" @endif>
                <a href="{{ route('me.registrations') }}">Реєстрації</a>
            </li>
        </ul>
    </div>
</div>