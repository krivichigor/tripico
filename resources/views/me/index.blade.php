@extends('layouts.app')

@section('content')

<div class="container me-page">
    @include('me._secondary_menu')

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive table-hover">
                <table class="table margin-bottom-40">
                    <thead>
                        <tr>
                            <td></td>
                            <td>Назва</td>
                            <td>Дата</td>
                            <td>Кіл-сть днів</td>
                            <td>Звідки</td>
                            <td>Реєстрацій</td>
                        </tr>
                    </thead>
                    @foreach($tours as $tour)
                        
                        <tr class="{{ $tour->is_archive ? 'disabled' : '' }}">
                            
                            <td width="70px"><img src="{{ $tour->image_thumb_url }}" height="40px"></td>
                            <td>
                                <a href="{{ route('me.tour.edit', $tour) }}">{{ $tour->title }}</a>
                            </td>
                            <td>{{ $tour->starts_at->format('d.m.y') }}</td>
                            <td>{{ $tour->days }}</td>
                            <td>{{ $tour->from->name }}</td>
                            <td>{{ $tour->registrations->count() }}</td>

                        </tr>
                        
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    
    
</div>

@endsection