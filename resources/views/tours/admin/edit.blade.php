@extends('layouts.app')

@section('content')

<div class="container admin-tours edit">
	@include('me._secondary_menu')		
	{!! Form::model($tour, ['url' => route('me.tour.update', $tour), 'method' => 'put', 'enctype'=>"multipart/form-data", 'class'=>'form-horizontal']) !!}
		@include('tours.admin._form')
	{!! Form::close() !!}
</div>

@endsection
