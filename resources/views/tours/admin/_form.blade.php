<div class="row form">
	<div class="col-lg-10 col-lg-offset-1 ">
		<div class="block-20 border-1">
			<div>

			  <!-- Nav tabs -->
			    <ul class="nav nav-tabs" role="tablist">
				    <li role="presentation"  class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab">Основні</a></li>
				    <li role="presentation"><a href="#price" aria-controls="price" role="tab" data-toggle="tab">Ціни</a></li>
				    <li role="presentation"><a href="#program" aria-controls="program" role="tab" data-toggle="tab">Програма</a></li>
				    <li role="presentation"><a href="#included" aria-controls="included" role="tab" data-toggle="tab">Включено</a></li>
			    </ul>
			</div>

			<!-- Tab panes -->
		    <div class="tab-content">
			    <div role="tabpanel" class="tab-pane fade in active" id="main">
				    
					<div class="row">
						<div class="col-md-8">
							<div class="form-group big">
								{{ Form::label('Назва туру', '', ['class' => 'col-sm-4 control-label']) }}
								<div class=" col-sm-8">
									{{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Новий Рік в Карпатах', 'required' => '']) }}
								</div>
							</div>

							

							<div class="form-group small">
								{{ Form::label('Дата подорожі', '', ['class' => 'col-sm-4 control-label']) }}
								<div class=" col-sm-4">
									{{ Form::date('starts_at', null, ['class' => 'form-control', 'placeholder' => '', 'required' => '']) }}
								</div>
								{{ Form::label('Днів', '', ['class' => 'col-sm-1 control-label']) }}
								<div class=" col-sm-3">
									
									{!! Form::select('days', range(1,20), null, ['class' => 'form-control ', 'required' => '']) !!}
								</div>
							</div>

							<div class="form-group small">
								{{ Form::label('Звідки', '', ['class' => 'col-sm-4 control-label']) }}
								<div class=" col-sm-8">
									{!! Form::select('from_id', $arr_to, null, ['class' => 'form-control select2', 'required' => '']) !!}
								</div>
							</div>

							<div class="form-group">
								{{ Form::label('Куди їдемо', '', ['class' => 'col-sm-4 control-label']) }}
								<div class=" col-sm-8">
									{!! Form::select('to[]', $arr_to, null, ['class' => 'form-control select2', 'multiple'=>'', 'required' => '']) !!}
								</div>
							</div>

							<div class="form-group small">
								{{ Form::label('Тип подорожі', '', ['class' => 'col-sm-4 control-label']) }}
								<div class=" col-sm-8">
									{!! Form::select('trip_types[]', $arr_trip_types, null, ['class' => 'form-control select2', 'multiple'=>'']) !!}
								</div>
							</div>

							<div class="form-group">
								{{ Form::label('Опис поїдки', '', ['class' => 'col-sm-4 control-label bold']) }}
								<div class=" col-sm-8">
									{!! Form::textarea('description',  null, ['class' => 'form-control', 'required' => '']) !!}
								</div>
							</div>

							


							
						</div>
						<div class="col-md-4 col-sm-offset-4 col-md-offset-0">
							<div class="form-group">
								<div class=" col-sm-12">
								
								
									<image_input name="upload_image" image-src="{{ ($tour) ? (($tour->image_url) ? : '') : '' }}"></image_input>
								</div>
								
							</div>
						</div>
					</div>


				</div>
				
				<div role="tabpanel" class="tab-pane fade" id="price">
					<div class="row">
						<div class="col-md-8">
							<div class="form-group ">
								{{ Form::label('Ціни', '', ['class' => 'col-sm-4 control-label big']) }}
								<div class=" col-sm-8">
									<tour-prices list_json="{{ ($tour) ? $tour->prices()->select('price', 'title', 'id')->get()->toJson() : '' }}"></tour-prices>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div role="tabpanel" class="tab-pane fade" id="program">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group ">
								
								<div class=" col-sm-12">
									{{ Form::label('Програма', '', ['class' => 'control-label big']) }}
									<tour-program list_json="{{ ($tour) ? json_encode($tour->program) : '' }}"></tour-prices>
								</div>
							</div>
						</div>
					</div>
				</div>


				<div role="tabpanel" class="tab-pane fade" id="included">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group ">
								
								<div class=" col-sm-6">
									{{ Form::label('Включено', '', ['class' => 'control-label big']) }}
									<tour-included list_json="{{ ($tour) ? json_encode($tour->included) : '' }}" name="included"></tour-included>
								</div>
								<div class=" col-sm-6">
									{{ Form::label('Невключено', '', ['class' => 'control-label big']) }}
									<tour-included list_json="{{ ($tour) ? json_encode($tour->excluded) : '' }}" name="excluded"></tour-included>
								</div>
							</div>
						</div>
					</div>
				</div>


			</div>

			<div class="row">
				<div class="col-md-8">
					<div class="form-group">
						<div class="col-sm-8 col-sm-offset-4">
						{{ Form::submit('Зберегти',  ['class' => "btn btn-warning btn-lg"]) }}
						</div>
					</div>
				</div>
			</div>

			

			

			

		</div>
	</div>
</div>


@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection


@section('scripts')
	<script>
		$('select.select2').select2();
	</script>
@endsection
