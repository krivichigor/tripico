@extends('layouts.app')

@section('content')

<div class="container admin-tours create" >
	@include('me._secondary_menu')
	{!! Form::open(['url' => route('me.tour.store'), 'method' => 'POST', 'enctype'=>"multipart/form-data", 'class'=>'form-horizontal']) !!}
		@include('tours.admin._form')
	{!! Form::close() !!}
</div>
@endsection
