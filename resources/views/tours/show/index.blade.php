@extends('layouts.app')

@section('content')

<div class="container page-one-tour" >
	<div class="row">
		<div class="col-xs-12">
			<ol class="breadcrumb">
			  <li><a href="{{ url('/') }}">Головна</a></li>
			  <li><a href="{{ route('tour.list') }}">Екскурсії</a></li>
			  <li class="active">{{ $tour->title }}</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-md-10">
			<div class="block-40 z-depth-1 content">
				<div class="title">
					<h1>{{ $tour->title }}</h1>
					<h3>
						<span class="small">
							@foreach ($tour->trip_types as $item)
								{{ $item->title }} 
							@endforeach
						</span>
					</h3>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<img src="{{ $tour->image_url }}" class="img-responsive">
					</div>
					<div class="col-lg-6">
						<div class="description">
							{!! $tour->description !!}
							
						</div>
						<div class="row margin-top-40  order-block">
			                <div class="col-sm-6 col-md-7 text-left text-center-xs margin-bottom-20" style="text-align:right;" >
			                    
			                    
			                    <li class="dropdown select-block"  v-cloak>
				                    <a href="#" class="dropdown-toggle price big" data-toggle="dropdown">
					                    {{ $tour->price_min->price }}
				                        <span class="currency small"> грн</span>
				                        <span class="caret"></span>
				                        <div class="description truncate">
				                        	{{ $tour->price_min->title }}
				                        </div>
			                        </a>
				                    <span class="dropdown-arrow"></span>
				                    <ul class="dropdown-menu">
				                    	@foreach ($tour->prices as $item)
								            <li >
					                        	<div class="p">
						                        	<span class="">{{ $item->price }}</span>
						                        	<span class="small"> грн</span>
					                        	</div>
					                        	<div class="description">
					                        		{{ $item->title }}
					                        	</div>
					                        </li>
					                    @endforeach
				                    </ul>
				                </li>
				                
			                    
			                </div>
			                <div class="col-sm-6 col-md-5 text-right text-center-xs">
			                    <a href="#" class="order big label-warning" data-toggle="modal" data-target=".registration-modal">
			                    	<span class="small">Реєстрація</span>
			                    </a>
			                </div>
			            </div>
					</div>
				</div>
				<div class="row options">
					<div class="col-md-12">
						<h4 class="headline margin-top-40"><span>Особливості</span></h4>
						<div class="row">
							<div class="col-xs-6 col-md-3 text-center">
								<svg height="48" viewBox="0 0 48 48" width="48" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h48v48h-48z" fill="none"/><path d="M8 32c0 1.77.78 3.34 2 4.44v3.56c0 1.1.9 2 2 2h2c1.11 0 2-.9 2-2v-2h16v2c0 1.1.89 2 2 2h2c1.1 0 2-.9 2-2v-3.56c1.22-1.1 2-2.67 2-4.44v-20c0-7-7.16-8-16-8s-16 1-16 8v20zm7 2c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm18 0c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm3-12h-24v-10h24v10z"/></svg>
								<div class="">Автобус з <a href="{{ route('tour.list', 'from=' . $tour->from->slug) }}">{{ $tour->from->name }}</a></div>
							</div>
							<div class="col-xs-6 col-md-3 text-center">
								<svg enable-background="new 0 0 512 512" height="48" version="1.1" viewBox="0 0 512 512" width="48" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Layer_2"><rect fill="#7ED746" height="512" width="512" x="538"/><rect fill="#2F75B5" height="512" width="512" x="-537"/></g><g id="Layer_1"><path d="M256,36.082c-84.553,0-153.105,68.554-153.105,153.106c0,113.559,153.105,286.73,153.105,286.73   s153.106-173.172,153.106-286.73C409.106,104.636,340.552,36.082,256,36.082z M256,253.787c-35.682,0-64.6-28.917-64.6-64.6   s28.918-64.6,64.6-64.6s64.6,28.917,64.6,64.6S291.682,253.787,256,253.787z" fill=""/></g></svg>
								<div class="">
										Їдемо в 
										@foreach ($tour->to as $item)
											<a href="{{ route('tour.list', 'to=' . $item->slug) }}">
													{{ $item->name }}</a>{{ ($loop->iteration != $loop->count ) ? ', ' : '' }}
										@endforeach
										</div>
							</div>
							<div class="col-xs-6 col-md-3 text-center">
								<svg enable-background="new 0 0 512 512" height="48" id="Layer_1" version="1.1" viewBox="0 0 512 512" width="48" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M357.784,369.481h-52.621v52.604h52.621V369.481z M283.644,228.695h-52.589v52.593h52.589V228.695z M357.784,299.087  h-52.621v52.598h52.621V299.087z M431.857,228.695h-52.591v52.593h52.591V228.695z M283.644,299.087h-52.589v52.598h52.589V299.087z   M357.784,228.695h-52.621v52.593h52.621V228.695z M431.857,299.087h-52.591v52.598h52.591V299.087z M135.442,369.481H82.847v52.604  h52.595V369.481z M135.442,299.087H82.847v52.598h52.595V299.087z M418.325,43.264v23.088c9.073,8.028,14.823,19.728,14.823,32.795  c0,24.192-19.619,43.813-43.815,43.813c-24.204,0-43.814-19.621-43.814-43.813c0-13.072,5.748-24.77,14.824-32.795V43.264H155.619  v23.088c9.074,8.028,14.823,19.728,14.823,32.795c0,24.192-19.618,43.813-43.815,43.813c-24.201,0-43.814-19.621-43.814-43.813  c0-13.072,5.75-24.77,14.823-32.795V43.264H0.001v466.244h512V43.264H418.325z M465.24,462.742H46.764V171.461H465.24V462.742  L465.24,462.742z M209.575,228.695h-52.624v52.593h52.624V228.695z M209.575,369.481h-52.624v52.604h52.624V369.481z   M283.644,369.481h-52.589v52.604h52.589V369.481z M209.575,299.087h-52.624v52.598h52.624V299.087z M126.629,124.673  c12.045,0,21.811-9.762,21.811-21.807V24.302c0-12.039-9.766-21.809-21.811-21.809c-12.047,0-21.812,9.771-21.812,21.809v78.564  C104.817,114.91,114.582,124.673,126.629,124.673 M389.335,124.673c12.043,0,21.811-9.762,21.811-21.807V24.302  c0-12.039-9.768-21.809-21.811-21.809c-12.046,0-21.813,9.771-21.813,21.809v78.564C367.52,114.91,377.287,124.673,389.335,124.673"/></svg>
								<div class="">
									{{ $tour->starts_at->format('d.m.y') }}
									<div class="small">{{ $tour->days }}-денний тур</div>
								</div>
							</div>
							<div class="col-xs-6 col-md-3 text-center">
								<svg enable-background="new 0 0 24 24" height="48" id="Layer_1" version="1.1" viewBox="0 0 24 24" width="48" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path clip-rule="evenodd" d="M21.652,3.211c-0.293-0.295-0.77-0.295-1.061,0L9.41,14.34  c-0.293,0.297-0.771,0.297-1.062,0L3.449,9.351C3.304,9.203,3.114,9.13,2.923,9.129C2.73,9.128,2.534,9.201,2.387,9.351  l-2.165,1.946C0.078,11.445,0,11.63,0,11.823c0,0.194,0.078,0.397,0.223,0.544l4.94,5.184c0.292,0.296,0.771,0.776,1.062,1.07  l2.124,2.141c0.292,0.293,0.769,0.293,1.062,0l14.366-14.34c0.293-0.294,0.293-0.777,0-1.071L21.652,3.211z" fill-rule="evenodd"/></svg>
								<div class="">Є вільні місця</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row program">
					<div class="col-md-12">
						<h4 class="headline margin-top-40"><span>План поїздки</span></h4>
						@foreach($tour->program as $day)
				            <div class="day margin-bottom-20">
				            	<h6><span class="label label-default">{{ $day->title }}</span></h6>
				            	<div class="text">{!! $day->description !!}</div> 
				            </div>
				        @endforeach
					</div>
				</div>
				<div class="row included margin-top-40">
					<div class="col-md-6">
						<h4 class="headline">
							<span>
								
								Включено
							</span>
						</h4>
						@foreach ($tour->included as $item) 
							<div>
								<span class="fui-check small text-success"></span>
								{{ $item->title }}
							</div>
						@endforeach
					</div>
					<div class="col-md-6">
						<h4 class="headline">
							<span>
								Не включено
							</span>
						</h4>
						@foreach ($tour->excluded as $item) 
							<div>
								<span class="fui-cross small text-danger"></span>
								{{ $item->title }}
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-2">
			@include('tours.show._organizator')
		</div>
	</div>


	@include('tours.show._registration')
</div>
@endsection

{{-- @section('scripts')
<script>
	new Vue({
        el: 'body',
        data: {
            price: 1240,
            price_title: 'одномісний номер',
        },
        ready: function () {
        	

			
        },
        methods: {
            setPrice: function (p, t) {
                this.price = p;
                this.price_title = t;
            },
        }
    });
</script>
@endsection --}}