<div class="modal fade registration-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-md" role="document">
	    <div class="modal-content ">
	     	<div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Реєстрація на "{{ $tour->title }}"</h4>
		    </div>
		    <div class="modal-body">
		         
		         {!! Form::open(['url' => route('tour.registration', $tour), 'method' => 'POST', 'enctype'=>"multipart/form-data", 'class'=>'form-horizontal']) !!}
		         	<div class="col-md-12">
					    <div class="form-group">
			                <input type="text" class="form-control login-field" value="" name="name" required placeholder="Введи ім'я" id="login-name">
			                <label class="login-field-icon fui-user" for="login-name"></label>
			            </div>

					    <div class="form-group">
					    	<div class="row">
					    		<div class="col-md-6">
							        <input name="phone" type="phone" class="form-control" placeholder="Номер телефону" required />
							        <label class="login-field-icon fui-phone" for="signin-phone" style="right: 30px;"></label>
							    </div>
						        <div class="col-md-6">
							        <input name="email" type="email" class="form-control" id="registerEmail" placeholder="Email" required />
							        <label class="login-field-icon fui-mail" for="signin-email" style="right: 30px;"></label>
							    </div>
							</div>
			            </div>
					    
					    <div class="form-group margin-bottom-10">
				        	<textarea name="comment" class="form-control" placeholder="Коментар, питання, побажання"></textarea>
					    </div>

					    <div class="form-group">
					    	<div class="row">
					    		
						        <div class="col-md-4">
						        	<label>Кількість людей</label>
						        	<select name="people_count" value="X-Men" class="form-control">
						                @for ($i = 1; $i < 11; $i++)
										    <option value="{{ $i }}">{{ $i }}</option>
										@endfor
						            </select>
						        </div>

						        <div class="col-md-8">
						        	<label>Опції поїздки</label>
						        	<select name="option" class="form-control">
						        		
						        		@foreach ($tour->prices as $item)
							                <option value="{{ $item->id }}">{{ $item->price }} грн - {{ $item->title }}</option>
						                @endforeach
						            </select>
						        </div>
					        </div>
					    </div>

					    

					    
				    </div>
				    <button type="submit" class="btn btn-primary btn-xl">Зареєструватись</button>
				{!! Form::close() !!}
		    </div>

		    <div class="modal-footer">
		    	<p>Після реєстрації, організатор сконтактує із вами.</p>
		    	<p>Реєстрація - безкоштовна, ви будете оплачувати вартість туру безпосередньо організатору</p>
		    </div>
	    </div>
    </div>
</div>