<div class="block-20 z-depth-1">
	<div class="block-label">Організатор</div>
	<div class="text-center">
		<div class="row">
			<div class="col-md-12 col-sm-6">
				<img src="{{ $tour->user->image_url }}" alt="..." class="img-circle" width="100%">
			</div>
			<div class="col-md-12 col-sm-6">
				<div class="margin-top-10">{{ $tour->user->name }}</div>
				<div class="small text-default text-muted">Київ, Україна</div>

				<div class="margin-top-10">Телефон</div>
				<div><phone_show phone="{{ $tour->user->phone }}"></phone_show></div>
			</div>
		</div>
	</div>
</div>
