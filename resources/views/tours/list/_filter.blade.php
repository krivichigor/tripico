<!-- Default skin  -->
<div class="row filter">
    <div class="col-sm-12 col-md-6 col-lg-3">
        <h5 class=" color-gray">Виїзжаємо з...</h5>
        <div class="btn-group select select-block">
            <button class="btn dropdown-toggle clearfix {{ (array_key_exists('from', $filter)) ? 'btn-primary' : 'btn-default' }}" data-toggle="dropdown">
                <span class="filter-option pull-left">
                    @if (array_key_exists('from', $filter))
                        {{ $filter['from']->name }}
                        <a href="{{ route('tour.list') }}" onClick="window.location = this.href">
                            <span class="fui-cross" style="margin-left:5px; color:white;"></span>
                        </a>
                    @else
                        Всі
                    @endif
                </span>&nbsp;
                <span class="caret"></span>
            </button>
            <span class="dropdown-arrow dropdown-arrow-inverse"></span>
            <ul class="dropdown-menu dropdown-inverse" role="menu">
                @foreach($location_from as $item)
                    <li rel="0">
                        <a tabindex="-1" href="{{ route('tour.list', 'from=' . $item->slug) }}" class="">
                            <span>{{ $item->name }}  <span class="badge pull-right">{{ $item->_count }}</span></span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>


    <div class="col-sm-12 col-md-6 col-lg-3">
        <h5 class=" color-gray">Їдемо в...</h5>
        <div class="btn-group select select-block">
            <button class="btn dropdown-toggle  {{ (array_key_exists('to', $filter)) ? 'btn-primary' : 'btn-default' }}" data-toggle="dropdown">
                <span class="filter-option pull-left">
                    @if (array_key_exists('to', $filter))
                        {{ $filter['to']->name }}
                        <a href="{{ route('tour.list') }}" onClick="window.location = this.href">
                            <span class="fui-cross" style="margin-left:5px; color:white;"></span>
                        </a>
                    @else
                        Всі
                    @endif
                </span>&nbsp;
                <span class="caret"></span>
            </button>

            <span class="dropdown-arrow dropdown-arrow-inverse"></span>
            <ul class="dropdown-menu dropdown-inverse" role="menu">
                @foreach($location_to as $item)
                    <li rel="0">
                        <a tabindex="-1" href="{{ route('tour.list', 'to=' . $item->slug) }}" class="">
                            <span>{{ $item->name }} <span class="badge pull-right">{{ $item->_count }}</span></span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>


    <div class="col-sm-12 col-md-6 col-lg-3">
        <h5 class=" color-gray">Тип подорожі...</h5>
        <div class="btn-group select select-block">
            <button class="btn dropdown-toggle clearfix {{ (array_key_exists('type', $filter)) ? 'btn-primary' : 'btn-default' }}" data-toggle="dropdown">
                <span class="filter-option pull-left">
                    @if (array_key_exists('type', $filter))
                        {{ $filter['type']->title }}
                        <a href="{{ route('tour.list') }}" onClick="window.location = this.href">
                            <span class="fui-cross" style="margin-left:5px; color:white;"></span>
                        </a>
                    @else
                        Всі
                    @endif
                </span>&nbsp;
                <span class="caret"></span>
            </button>
            <span class="dropdown-arrow dropdown-arrow-inverse"></span>
            <ul class="dropdown-menu dropdown-inverse" role="menu">
                @foreach($trip_types as $item)
                    <li rel="0">
                        <a tabindex="-1" href="{{ route('tour.list', 'type=' . $item->slug) }}" class="">
                            <span>{{ $item->title }} <span class="badge pull-right">{{ $item->_count }}</span></span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

</div>