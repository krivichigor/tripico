
<div class="card hoverable z-depth-1">
    <div class="href card-image">
        <a href="{{ route('tour.show', $tour->id) }}">
            <img src="{{ $tour->image_url }}" class="photo">
       
            <span class="card-title ">
                {{ str_limit($tour->title, 50) }}

            </span>
            <div class="shadow"></div>
        </a>
        <div class="date">
            {{ $tour->starts_at->format('d.m.y') }}
        </div>
    </div>
    <div class="company font-12 text-right" >
        <a href="#">
            <span  style="">{{ $tour->user->name }}</span>
        </a>
    </div>

    <div class="card-body">
        <div class="row options">
            <div class="col-xs-4 " >
                Виїзд з:
                    <a href="{{ route('tour.list', 'from=' . $tour->from->slug) }}">
                    {{ $tour->from->name }} 
                    </a>
            </div>
            <div class="col-xs-6 " style="padding-left: 0;">
                Їдемо в:
                @foreach($tour->to as $item)
                    <a href="{{ route('tour.list', 'to=' . $item->slug) }}">
                        {{ $item->name }}{{ ($loop->iteration != $loop->count ) ? ', ' : '' }}
                    </a>
                @endforeach
            </div>
            <div class="col-xs-2" style="padding-left: 0;">
                {{ $tour->days }} {{ ($tour->days == 1) ? 'день' : (($tour->days > 5) ? 'днів' : 'дні')  }}
            </div>
        </div>
        <p>
            {{ str_limit($tour->description, 100) }}
        </p>
        <div class="row">
            <div class="col-xs-6 text-left">
                <span class="price label label-warning">
                    {{ $tour->price_min->price }}
                    <span class="currency small"> грн</span>
                </span>
            </div>
            <div class="col-xs-6 text-right">
                <a href=""  class="btn btn-primary">
                    Детальніше
                </a>
            </div>
        </div>
    </div>
    
</div>
