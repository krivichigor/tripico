@extends('layouts.app')

@section('content')

<div class="container one-page" data-js="catalog">


    <div class="row search-title">
        <div class="col-xs-12 text-center">
            <h1 class="margin-bottom-20">{{ ($h1_text) ? $h1_text : 'Всі подорожі по Україні' }}</h1>
            <p class="color-gray">Знайдено
                {{ count($tours) }}
                актуальних турів</p>
                
        </div>
    </div>

    @include('tours.list._filter')
    
    <br>
    <div class="row">
        @foreach ($tours as $tour)
            <div class=" col-xs-12 col-sm-6 col-md-4">
                @include('tours.list._card')
            </div>
        @endforeach
    </div>
    
</div>

@endsection