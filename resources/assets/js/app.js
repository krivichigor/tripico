
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('phone_show', require('./components/PhoneShow.vue'));
Vue.component('image_input', require('./components/ImageInput.vue'));
Vue.component('tour-prices', require('./components/TourPrices.vue'));
Vue.component('tour-program', require('./components/TourProgram.vue'));
Vue.component('tour-included', require('./components/TourIncluded.vue'));

const app = new Vue({
    el: '#layout-content',
    ready: function() {
        
    },
});
